import java.util.Arrays;
import java.util.Scanner;

public class RleProgram {
    static Scanner input =new Scanner(System.in);

    // Method to Display Main Menu
    public static int DisplayMenu(){
        System.out.println("File Menu\n---------");
        System.out.println("0. Exit");
        System.out.println("1. Load File");
        System.out.println("2. Load Test Image");
        System.out.println("3. Read RLE String");
        System.out.println("4. Read RLE Hex String");
        System.out.println("5. Read RLE Data Hex String");
        System.out.println("6. Display Image");
        System.out.println("7. Display RLE String");
        System.out.println("8. Display Hex RLE Data");
        System.out.println("9. Display Hex Flat Data\n");
        System.out.println("Select a Menu Option:\n");
        //int opt=input.nextInt();
        return input.nextInt();
    }

    // Method -1: encode Byte vector to Hexadecimal string
    public static String toHexString(byte[] data){
        String str="";
        for (int j=0; j<data.length;j++){
            if(data[j]>=0 && data[j]<=15)
                str=str+Integer.toHexString(data[j]);
            else {
                System.out.println("Byte value out of bounds (should be between 0 and 15 only)");
                System.exit(0);
            }
        }
        return str;
    }
    //Method -2 Daddy:  to return distinct values in a byte data array
    /*public static int countRuns(byte[] flatData){
        int count=1;
        int subcount=1;
        //byte
        for (int j=1;j<flatData.length;j++){
                //count = flatData[j - 1] == flatData[j] ? count = count : count + 1;
            if (subcount>16){
                count++;
                subcount=1;
0            }
            if(flatData[j - 1] == flatData[j]){
                subcount++;
            }
            else {
                count++;
                subcount++;
            }

         }
        return count;
    }*/
    // Method2: Rida version
    // takes a byte array and returns an int value that gives the number of different values that appear at a time
    public static int countRuns(byte[] flatdata) {
        int nums = 0;
        int a = 0;
        int b = 0;
        for(a = 1 ; a < flatdata.length ; a++) // looks through each value in the array
        {
            if (flatdata[a] == flatdata[a - 1] && b < 14) // compares all adjacent values and counts the one that differs
            {
                b = b + 1;
                continue;
            } else {
                nums = nums + 1; // adds up all places where the value changes, giving different sets of data
                b = 0;
            }

        }
        nums = nums + 1;
        return nums;
    }

    // Method -3: Encode RLE Byte vector from Flat byte vector
    public static byte[] encodeRle(byte[] flatdata){
        int nums=countRuns(flatdata);
        byte[] rlevector=new byte[nums*2];
        int tcount=1;
        //int tvale=0;
        int setindex=0;
        for (int j=0;j<=flatdata.length-1;j++){
            if (j==flatdata.length-1){
                rlevector[setindex]=(byte) tcount;
                rlevector[setindex+1]=flatdata[j];
            }
            else if(flatdata[j+1]==flatdata[j] && tcount<15) {
                tcount += 1;
            }
            else {
                rlevector[setindex]=(byte) tcount;
                rlevector[setindex+1]=flatdata[j];
                setindex+=2;
                tcount=1;
             }

        }
        return rlevector;
    }

    //Method -4: get decoded length from encoded rledata
    public static int getDecodedLength(byte[] rleData){
        int count=0;
        for(int j=0;j<rleData.length;j++){
            if(j%2==0)
                count+=rleData[j];
        }
        return count;
    }

    //Method -5: Expand/Decode encoded Rle data to full length
    public static byte[] decodeRle(byte[] rleData){
        int len=rleData.length/2;
        int bytlen=getDecodedLength(rleData);
        int runcount=0;
        byte[] decbyte=new byte[bytlen];
        for(int j=0;j<len;j++){
            int val=rleData[j*2];
            for(int k=0;k<val;k++){
                decbyte[runcount]=rleData[2*j+1];
                runcount++;
            }
        }
        return decbyte;
    }

    //Method -6: convert encoded hex to byte array
    public static byte[] stringToData(String dataString) {
        int len = dataString.length();
        byte[] byteArray = new byte[len];
        char[] charArray = dataString.toCharArray();
       // System.out.println(Arrays.toString(charArray));
        for (int j = 0; j < charArray.length; j++) {
            if (charArray[j] >= '0' && charArray[j] <= '9')
             byteArray[j] = (byte) Character.getNumericValue(charArray[j]);
            else if(charArray[j]=='a')
                byteArray[j]=10;
            else if(charArray[j]=='b')
                byteArray[j]=11;
            else if(charArray[j]=='c')
                byteArray[j]=12;
            else if(charArray[j]=='d')
                byteArray[j]=13;
            else if(charArray[j]=='e')
                byteArray[j]=14;
            else if(charArray[j]=='f')
                byteArray[j]=15;
         }
        return byteArray;
    }

    //Method -7: Translate Rle encoded byte to Dec/Hex string
    public static String toRleSting(byte[] rleData){
        String rlestr = "";
        for(int j=0;j<rleData.length;j+=2){
            if(rleData[j+1]>=0 && rleData[j+1]<=9)
                rlestr=rlestr+rleData[j]+rleData[j+1];
            else if(rleData[j+1]==10)
                rlestr=rlestr+rleData[j]+"A";
            else if(rleData[j+1]==11)
                rlestr=rlestr+rleData[j]+"B";
            else if(rleData[j+1]==12)
                rlestr=rlestr+rleData[j]+"C";
            else if(rleData[j+1]==13)
                rlestr=rlestr+rleData[j]+"D";
            else if(rleData[j+1]==14)
                rlestr=rlestr+rleData[j]+"E";
            else if(rleData[j+1]==15)
                rlestr=rlestr+rleData[j]+"F";
            if(j!=rleData.length-2)
            rlestr=rlestr+":";

        }
        return rlestr;
    }

    //Method -8: Convert rleString to rleByte data format
    public static byte[] stringToRle(String rleString){
        String newstr="";
        String[] newrlestr=rleString.split(":");
        for (String onestr : newrlestr) {
            newstr=newstr+onestr.substring(0,onestr.length()-1)+" "+onestr.substring(onestr.length()-1,onestr.length())+" ";
        }
        System.out.println(newstr);
        newstr=newstr.replace("a","10");
        newstr=newstr.replace("b","11");
        newstr=newstr.replace("c","12");
        newstr=newstr.replace("d","13");
        newstr=newstr.replace("e","14");
        newstr=newstr.replace("f","15");
        System.out.println(newstr);
        String[] tempstrarry=newstr.split(" ");
        byte[] byteStr=new byte[tempstrarry.length];
        for(int j=0;j<byteStr.length;j++){
            byteStr[j]= Byte.parseByte(tempstrarry[j]);
        }
        return byteStr;
    }

    // Main method - Entry point
    public static void main(String[] args) {
        //test method 1
        byte[] raw={1,2 ,3 ,4,5, 6 ,7 ,8 ,9 ,10 ,11, 12 ,13, 14, 15,0};
        System.out.println("Method1: "+toHexString(raw));


        byte[] raw2={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2};
        byte[] raw3={15,15,6,4,11,3,4,13};
        byte[] raw4={5, 5, 7, 7, 7, 7, 7, 5, 5, 5, 5, 5, 2, 2, 0, 0};
        String str6="abc0d1234";
        System.out.println("Method2: "+countRuns(raw2));
        System.out.println("Method3: "+Arrays.toString(encodeRle(raw2)));
        System.out.println("Method4: "+getDecodedLength(raw4));
        System.out.println("Method5: "+Arrays.toString(decodeRle(raw3)));
        System.out.println("Mehthod 6: "+Arrays.toString(stringToData(str6)));
        System.out.println("Mehthod 7: "+toRleSting(raw3));
        System.out.println("Mehthod 8: "+Arrays.toString(stringToRle("15f:64:56:7b")));
        System.exit(0);
        //display welcome message
        System.out.println("Welcome to RLE program");
        // Display color test with message
        System.out.println("Test Image. Adjust or import color scheme as needed");
        ConsoleGfx.displayImage(ConsoleGfx.testRainbow);
        byte[] imageData = null;
        int MenuOption = DisplayMenu();
        Scanner input = new Scanner(System.in);
        String tempStr = "";
        // Run case based on user input
        while (MenuOption != 0) {
            switch (MenuOption) {
                case 1:
                    System.out.println("Enter name of file to load");
                    tempStr = input.next();
                    imageData = ConsoleGfx.loadFile(tempStr);
                    break;
                case 2:
                    imageData = ConsoleGfx.testRainbow;
                    System.out.println("Test image data loaded");
                    break;
                case 3:
                    System.out.println("Not implemented yet.");
                    break;
                case 4:
                    System.out.println("Not implemented yet.");
                    break;
                case 5:
                    System.out.println("Not implemented yet.");
                    break;
                case 6:
                    ConsoleGfx.displayImage(imageData);
                    break;
                case 7:
                    System.out.println("Not implemented yet.");
                    break;
                case 8:
                    System.out.println("Not implemented yet.");
                    break;
                case 9:
                    System.out.println("Not implemented yet.");
                    break;
                default:
                    System.out.println("Something went wrong. No correct option");
            }
            MenuOption = DisplayMenu();
        }
        System.out.println("Good Bye!!!");
    }
}


